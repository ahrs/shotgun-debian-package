DESTDIR?=debian/shotgun

all: build install

build:
	env RUST_BACKTRACE=1 cargo build --release

install:
	install -dm755 "$(DESTDIR)/usr/bin"
	install -m755 target/release/shotgun "$(DESTDIR)/usr/bin"
